# OEMKey

The **Windows OEM key** allows the activation of the Windows product, without this key you cannot use the services of the Windows Operating System.

* Retrieve the **OEM key** from Windows if you installed Linux via a grub or if you deleted Windows to set up Linux.
* Retrieves the **OEM key** from Windows on Windows.

## Dependencies

* Python 3 (https://www.python.org/downloads/)

## Terminal

### Linux
```
    chmod +x OEMKey.py
    sudo ./OEMKey.py
```

#### You may encounter this error under Linux
* If you run the script on Linux and it indicates an error that tells you **"Wrong interpreter"**, do this

#### How to correct the wrong interpreter error ?

* **chmod +x** OEMKey.py
* **aptitude** install **dos2unix** or **apt-get** install **dos2unix**
* **dos2unix** OEMKey.py
* **sudo** ./OEMKey.py or **sudo python3** OEMKey.py

Normally it should work without any problem :)

#### Why the mistake of Bad Interpreter?

* This error is due to the fact that we program on a Windows and run on a Linux, it may be that at some point it causes the error of "Bad interpreter".

### Windows 
```
    python OEMKey.py OR OEMKey.py
```

## Contact

Feel free to contact me if you have any **questions**, **criticisms**, **suggestions**, **bugs encountered**

* **Mail :** mx.moreau1@gmail.com
* **Twitter :** https://twitter.com/mxmmoreau
* **LinkedIn :** https://www.linkedin.com/in/mxmoreau/