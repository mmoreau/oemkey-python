#!/usr/bin/python3

import os
import subprocess as sub
import platform as plf

if plf.system() == "Linux":
    if os.getuid() == 0:
        print(sub.Popen("cat /sys/firmware/acpi/tables/MSDM | tail -c 32", shell=True, stdin=sub.PIPE, stdout=sub.PIPE, stderr=None).communicate()[0].decode())
    else:
        print("You must have root rights.")
elif plf.system() == "Windows":
	print(sub.Popen("powershell \"(Get-WmiObject -query ‘select * from SoftwareLicensingService’).OA3xOriginalProductKey\"", shell=True, stdin=sub.PIPE, stdout=sub.PIPE, stderr=None).communicate()[0].decode())
else:
    print("You must be on Linux or Windows.")